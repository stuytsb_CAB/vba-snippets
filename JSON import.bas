'IMPORTANT: In VBA Code Editor Window, Include a reference to Microsoft Script Control 1.0
'Tools>References>Microsoft Script Control 1.0
'Browse for it on C:\Windows\SysWOW64\msscript.ocx on 64-bit machines
'              or C:\Windows\System32\msscript.ocx on 32-bit machines

Option Explicit

'-------------------------------------------------------------------------------
' Basic functions and subroutines to be included on all .json reading projects
'-------------------------------------------------------------------------------

Private ScriptEngine As ScriptControl

Public Sub InitScriptEngine()
    Set ScriptEngine = New ScriptControl
    ScriptEngine.Language = "JScript"
    ScriptEngine.AddCode "function getProperty(jsonObj, propertyName) { return jsonObj[propertyName]; } "
    ScriptEngine.AddCode "function getKeys(jsonObj) { var keys = new Array(); for (var i in jsonObj) { keys.push(i); } return keys; } "
End Sub

Public Function DecodeJsonString(ByVal JSONString As String)
    Set DecodeJsonString = ScriptEngine.Eval("(" + JSONString + ")")
End Function

Public Function GetProperty(ByVal JsonObject As Object, ByVal propertyName As String) As Variant
    GetProperty = ScriptEngine.Run("getProperty", JsonObject, propertyName)
End Function

Public Function GetObjectProperty(ByVal JsonObject As Object, ByVal propertyName As String) As Object
    Set GetObjectProperty = ScriptEngine.Run("getProperty", JsonObject, propertyName)
End Function

Public Function GetKeys(ByVal JsonObject As Object) As String()
    Dim Length As Integer
    Dim KeysArray() As String
    Dim KeysObject As Object
    Dim Index As Integer
    Dim Key As Variant

    Set KeysObject = ScriptEngine.Run("getKeys", JsonObject)
    Length = GetProperty(KeysObject, "length")
    ReDim KeysArray(Length - 1)
    Index = 0
    For Each Key In KeysObject
        KeysArray(Index) = Key
        Index = Index + 1
    Next
    GetKeys = KeysArray
End Function

Function OpenTextFileToString(ByVal strFile As String) As Variant
' RB Smissaert - Author
Dim hFile As Long
hFile = FreeFile
Open strFile For Input As #hFile
OpenTextFileToString = Input$(LOF(hFile), hFile)
Close #hFile
End Function

'-------------------------------------------------------------------------------
' Example .json reading code
'-------------------------------------------------------------------------------

Public Sub ImportJSON()

    InitScriptEngine

    'Browse for .cssn file with output from CARRIE
    Dim JSONfile As FileDialogSelectedItems
    Application.FileDialog(msoFileDialogOpen).Show
    Set JSONfile = Application.FileDialog(msoFileDialogOpen).SelectedItems
    JSONfilename = JSONfile.Item(1)

    'Read .cssn to string
    Dim JSONString As Variant
    JSONString = OpenTextFileToString(JSONfilename)
    
    'Create JSON object from string
    Dim JsonObject As Object
    Dim LoadCaseKeys() As String
    Set JsonObject = DecodeJsonString(CStr(JSONString))
    LoadCaseKeys = GetKeys(JsonObject)
    
    'Example .json file
    '{"general": {
    '   "L": 12.0,
    '   "D": 6.0,
    '   "Suo1": [
    '       1.5,
    '       1.0,
    '       2.0,
    '       1.75
    '   ]}
    '}
    
    Dim GeneralObject As Object
    Set GeneralObject = GetObjectProperty(JsonObject, "general")
    
    Dim L,D as Double
    
    L = GetProperty(GeneralObject, "L")
    D = GetProperty(GeneralObject, "D")
    
    Dim m as Long
    Dim Suo1_val(4) as Double
    
    For m = 1 To 4
        Suo1_val(CLng(m)) = GetProperty(GetObjectProperty(GeneralObject, "Suo1"), m - 1)
    Next m
    
End Sub