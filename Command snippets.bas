Sub WritePDFDirect()
'Code for direct pdf printing (no dialogs) implemented by FVA

  PsPathandName = Directory & "\" & Name
  Sheets("PY St-a").ExportAsFixedFormat xlTypePDF, PsPathandName
  
End Sub

Sub Errorhandler ()
'Code for handling error in VBA

  On Error GoTo EH1
  Dim x As Double
  x = 1/0	'This will cause an error
  Exit Sub	

           
EH1:
  Msgbox("Error occured")

End Sub



Sub NA_error()
  '***************************************************************
  'Code for putting the value #N/A in a cell
  '***************************************************************
  Range("A1") = CVErr(xlerrna)
End Sub

Sub WriteDataToFile()
  '***************************************************************
  'Write data to a file
  '***************************************************************
  Set fs = CreateObject("Scripting.FileSystemObject")
  Set a = fs.CreateTextFile(Directory & "\" & filename & ".txt", True)
  a.WriteLine ("text")
  a.Close
End Sub

Sub LoopOverFiles()
  '***************************************************************
  'Loop over all .xls files in a directory
  '***************************************************************
  Directory = InputBox("Print directory", "Please input the directory with the source files")
  ChDrive Left(Directory, 1)
  ChDir Directory
  FName = Dir("*.xls")
  Do While FName <> ""
        Set WB = Workbooks.Open(Filename:=FName, ReadOnly:=True)
        WB.Activate
        'Some operations
        WB.close
        FName = Dir()
  Loop
End Sub


Sub ImportCAISSONResults()
  '***************************************************************
  'Select a file for opening and read line by line
  '***************************************************************
    Dim CAISSONfilename As Object

    'This block of code opens a file selection dialog and writes the name of the selected file to the variable CAISSONfilename
    Dim CAISSONfile As FileDialogSelectedItems
    Application.FileDialog(msoFileDialogFilePicker).Show()
    CAISSONfile = Application.FileDialog(msoFileDialogFilePicker).SelectedItems
    CAISSONfilename = CAISSONfile.Item(1)

    Dim iFileNum As Integer

    'Open the file with filename CAISSONfilename for reading and read line by line
    iFileNum = FreeFile()
    Open CAISSONfilename For Input As iFileNum
    Do While Not EOF(iFileNum)
        'sBuf contains one line from the file
        Line Input #iFileNum, sBuf
        'Test whether the character "," is included in the line
        If InStr(1, sBuf, ",", vbTextCompare) Then
            'Split the line using the character "," as delimiter, the strings between the commas are stored in an array
            If Split(sBuf, ",")(1) = "None" Then
                Range(Split(sBuf, ",")(0)) = ""
            Else
                For i = 1 To UBound(Split(sBuf, ","))
                    Range(Split(sBuf, ",")(0))(1, i) = Split(sBuf, ",")(i)
                Next i
            End If
        End If
    Loop
    Close(iFileNum)

End Sub


Sub PivotTableLoop()
  'Loops through all location elements in one PivotTable and ensures
  'the other PivotTables have the same location selected before printing
  'a .pdf figure
  Dim WSh As Worksheet
  Dim PT_LatDKP, PT_LatAPI, PT_MakeUp As PivotTable
  Set PT_LatDKP = Sheets("PivotTables").PivotTables("PY_API_Table")
  Set PT_LatAPI = Sheets("PivotTables").PivotTables("LateralSoilProfile")
  Set PT_MakeUp = Sheets("PivotTables").PivotTables("PileMakeUp")
  
  Dim Location As PivotItem
  Dim CheckLocation As PivotItem
     
  For Each Location In PT_LatDKP.PivotFields("Location").PivotItems
      
      Location.Visible = True
      For Each CheckLocation In PT_LatDKP.PivotFields("Location").PivotItems
          If Location <> CheckLocation And CheckLocation.Visible = True Then
              CheckLocation.Visible = False
          End If
      Next
      
      PT_LatAPI.PivotFields("Location").PivotItems(Location.Name).Visible = True
      For Each CheckLocation In PT_LatAPI.PivotFields("Location").PivotItems
          If Location <> CheckLocation And CheckLocation.Visible = True Then
              CheckLocation.Visible = False
          End If
      Next
      
      PT_MakeUp.PivotFields("Location").PivotItems(Location.Name).Visible = True
      For Each CheckLocation In PT_MakeUp.PivotFields("Location").PivotItems
          If Location <> CheckLocation And CheckLocation.Visible = True Then
              CheckLocation.Visible = False
          End If
      Next
      
      Application.ActivePrinter = "ScanSoft PDF Create! on Ne00:"
          ActiveWindow.SelectedSheets.PrintOut , To:=1, Copies:=1, ActivePrinter:= _
          "ScanSoft PDF Create! on Ne00:", Collate:=True
      
      
    Next
End Sub    
    
    
Sub RunProgram()
  ' Runs OPILE from inside VBA
    Program = "C:\Program Files (x86)\Cathie Associates\OPILE\OPILE.EXE"
    EquivOPILExfile = OPILEDirectory & "\EquivPile_x_D=" & Round(Sheets("Input").Range("N8"), 2) & "m_" & FileName
    
    CmdLine = Program & " " & EquivOPILExfile
    
    Dim TaskID As Variant

    TaskID = Shell(CmdLine, 1)

    If TaskID = 0 Then
        Errorstring = "HELP, I did not run"
    Else
        Errorstring = ""
    End If
End Sub


'Place this code in VBAProject>Microsoft Excel Objects>ThisWorkbook
Private Sub Workbook_Open()
    ' Will call the subroutine SubInModule defined in one of the VBA modules
    Call SubInModule

End Sub